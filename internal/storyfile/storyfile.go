package storyfile

import (
	"encoding/json"
)

// Arc represents one arc from the story's json file.
type Arc struct {
	Title   string   `json:"title"`
	Story   []string `json:"story"`
	Options []Option `json:"options"`
}

// Option represents one option from the story's json file
type Option struct {
	Text string `json:"text"`
	Arc  string `json:"arc"`
}

// UnmarshallJSON will take a slice of bytes and convert it into a
// map[string]arc objects -- where the key of the map is the title of the arc.
func UnmarshallJSON(b []byte) map[string]Arc {
	m := make(map[string]Arc)
	json.Unmarshal(b, &m)
	return m
}
