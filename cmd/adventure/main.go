package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"gitlab.com/mai-public/gophercises/adventure/internal/storyhttp"

	"github.com/gorilla/mux"

	"gitlab.com/mai-public/gophercises/adventure/internal/storyfile"
)

func main() {
	// Vars used for this project that might change
	// homedir, err := os.UserHomeDir()
	// if err != nil {
	// 	panic(err)
	// }
	gopath, ok := os.LookupEnv("GOPATH")
	if ok != true {
		log.Fatal("gopath not found")
	}
	var storyPath = gopath + string(os.PathSeparator) + "src" +
		string(os.PathSeparator) + "gitlab.com" +
		string(os.PathSeparator) + "mai-public" +
		string(os.PathSeparator) + "gophercises" +
		string(os.PathSeparator) + "adventure" +
		string(os.PathSeparator) + "gopher.json"

	// Get bytes from json filepath
	storyBytes, err := ioutil.ReadFile(storyPath)
	if err != nil {
		log.Fatalf("Issue with reading from json path %s.\nError: %v", storyPath, err)
	}
	// parse json
	storyJSON := storyfile.UnmarshallJSON(storyBytes)
	// Build webpage for each arc using html/template
	r := mux.NewRouter()
	for name, arc := range storyJSON {
		path := "/" + name
		// Build handler using template
		templateHandler := storyhttp.Template(name, arc)
		// assign handler to specified path
		r.Handle(path, templateHandler)
	}
	// Make '/' redirect to '/intro'
	r.Handle("/", storyhttp.RootRedirect("/intro"))

	// Serve content
	log.Println("Serving http")
	http.ListenAndServe(":3000", r)
}
