package storyhttp

import (
	"html/template"
	"log"
	"net/http"

	"gitlab.com/mai-public/gophercises/adventure/internal/storyfile"
)

// Template returns a handler that shows the html page associated with the given Arc
func Template(title string, arc storyfile.Arc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tmpl, err := template.New(title).Parse(`
<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{.Title}}</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body style="background-color: #333;">
<div class="jumbotron text-center">
  	<h1>{{.Title}}</h1>
 	{{range .Story}} 
  	<p>{{.}}</p>
	{{end}}
</div>
<div class="container">
	<div class="row">
	{{range .Options}}
	<div class="col-sm-4 d-block mx-auto text-center text-white" style="float: none; text-shadow: 0rem .05rem .1rem rgba(0, 0, 0, .5)">
		<h3>Option</h3>
		<p>{{.Text}}</p>
		<p1><a href="/{{.Arc}}" class="btn btn-lg btn-secondary" style="background-color: #fff; color: #333; font-weight: 600; padding: .75rem 1.25rem;">Select</a></p1>
	</div>
	{{end}}
	</div>
</div>
</body>
</html>
		`)
		if err != nil {
			log.Fatalf("Error when creating template: %v", err)
		}
		tmpl.Execute(w, &arc)
	})
}

// RootRedirect will take a specified path, and make the root page '/' redirect to that path
func RootRedirect(redirectPath string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, redirectPath, http.StatusFound)
	})
}
